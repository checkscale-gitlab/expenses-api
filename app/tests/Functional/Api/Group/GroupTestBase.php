<?php

namespace App\Tests\Functional\Api\Group;

use App\Tests\Functional\TestBase;
use Doctrine\ORM\Tools\ToolsException;

class GroupTestBase extends TestBase {

	protected string $endPoint;

	/**
	 * @throws ToolsException
	 */
	public function setUp(): void {
		parent::setUp();

		$this->endPoint = '/api/v1/groups';
	}
}
