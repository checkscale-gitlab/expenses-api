<?php

namespace App\Tests\Functional\Api\Group;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class GetGroupCategoriesTest extends GroupTestBase {

	/**
	 * Test get an owned group categories
	 */
	public function testGetGroupCategories(): void {
		$response = $this->makeRequest(self::IDS['user_group_id']);
		$responseData = $this->getResponseData($response);

		$this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
		$this->assertCount(1, $responseData['hydra:member']);
	}

	/**
	 * Test forbidden access (no get any category) from another user group
	 */
	public function testGetAnotherGroupCategories(): void {
		$response = $this->makeRequest(self::IDS['admin_category_id']);
		$responseData = $this->getResponseData($response);

		$this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
		$this->assertCount(0, $responseData['hydra:member']);
	}

	/**
	 * Make a get request given an id
	 *
	 * @param string $id
	 *
	 * @return Response
	 */
	private function makeRequest(string $id): Response {
		self::$user->request(
			'GET',
			sprintf(
				'%s/%s/categories.%s',
				$this->endPoint,
				$id,
				self::FORMAT
			)
		);

		return self::$user->getResponse();
	}
}
