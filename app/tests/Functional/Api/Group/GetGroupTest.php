<?php

namespace App\Tests\Functional\Api\Group;

use Symfony\Component\HttpFoundation\JsonResponse;

class GetGroupTest extends GroupTestBase {

	/**
	 * Test get all groups with an admin user
	 */
	public function testGetGroupsForAdmin():void {
		self::$admin->request(
			'GET',
			sprintf('%s.%s', $this->endPoint, self::FORMAT)
		);

		$response = self::$admin->getResponse();
		$responseData = $this->getResponseData($response);

		$this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
		$this->assertCount(2, $responseData['hydra:member']);
	}

	/**
	 * Test forbidden access to get all groups with a normal user
	 */
	public function testGetGroupsForUser():void {

		self::$user->request(
			'GET',
			sprintf('%s.%s', $this->endPoint, self::FORMAT)
		);

		$response = self::$user->getResponse();

		$this->assertEquals(JsonResponse::HTTP_FORBIDDEN, $response->getStatusCode());

	}

	/**
	 * Test get another user group data with an admin user
	 */
	public function testGetUserGroupWithAdmin(): void {
		self::$admin->request(
			'GET',
			sprintf(
				'%s/%s.%s',
				$this->endPoint,
				self::IDS['user_group_id'],
				self::FORMAT
			)
		);

		$response = self::$admin->getResponse();
		$responseData = $this->getResponseData($response);

		$this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
		$this->assertEquals(self::IDS['user_group_id'], $responseData['id']);
	}

	/**
	 * Test forbidden access to another user group data with a normal user
	 */
	public function testGetUserGroupWithUser(): void {
		self::$user->request(
			'GET',
			sprintf(
				'%s/%s.%s',
				$this->endPoint,
				self::IDS['admin_group_id'],
				self::FORMAT
			)
		);

		$response = self::$user->getResponse();

		$this->assertEquals(JsonResponse::HTTP_FORBIDDEN, $response->getStatusCode());
	}

}
