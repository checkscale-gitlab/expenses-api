<?php

namespace App\Tests\Functional\Api\User;

use Symfony\Component\HttpFoundation\JsonResponse;

class GetUserTest extends UserTestBase {

	/**
	 * Test get all users with an admin user
	 */
	public function testGetUsersWithAdmin(): void {
		self::$admin->request(
			'GET',
			sprintf('%s.%s',$this->endpoint,self::FORMAT )
		);

		$response = self::$admin->getResponse();
		$responseData = $this->getResponseData($response);

		$this->assertEquals(
			JsonResponse::HTTP_OK,
			$response->getStatusCode()
		);

		$this->assertCount(3, $responseData['hydra:member']);
	}

	/**
	 * Test forbidden access to get all users with a normal user
	 */
	public function testGetUsersWithUser(): void {
		self::$user->request(
			'GET',
			sprintf('%s.%s',$this->endpoint,self::FORMAT )
		);

		$response = self::$user->getResponse();

		$this->assertEquals(
			JsonResponse::HTTP_FORBIDDEN,
			$response->getStatusCode()
		);
	}

	/**
	 * Test get user data with an admin user
	 */
	public function testGetUserWithAdmin(): void {
		self::$admin->request(
			'GET',
			sprintf('%s/%s.%s', $this->endpoint,self::IDS['user_id'] ,self::FORMAT )
		);

		$response = self::$admin->getResponse();
		$responseData = $this->getResponseData($response);

		$this->assertEquals(
			JsonResponse::HTTP_OK,
			$response->getStatusCode()
		);

		$this->assertEquals(self::IDS['user_id'], $responseData['id']);
	}

	/**
	 * Test forbidden access to get an user with a normal user
	 */
	public function testGetAdminWithUser(): void {
		self::$user->request(
			'GET',
			sprintf('%s/%s.%s', $this->endpoint, self::IDS['admin_id'], self::FORMAT )
		);

		$response = self::$user->getResponse();

		// 403 response check
		$this->assertEquals(
			JsonResponse::HTTP_FORBIDDEN,
			$response->getStatusCode()
		);
	}
}
