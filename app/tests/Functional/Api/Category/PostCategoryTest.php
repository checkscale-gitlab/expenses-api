<?php

namespace App\Tests\Functional\Api\Category;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class PostCategoryTest extends CategoryTestBase {

	/**
	 * Test create a category
	 */
	public function testCreateCategory(): void {
		$payload = [
			'name' => 'Admin\'s category test',
			'user' => sprintf('/api/v1/users/%s', self::IDS['admin_id']),
		];

		$response = $this->makeRequest($payload);
		$responseData = $this->getResponseData($response);

		$this->assertEquals(JsonResponse::HTTP_CREATED, $response->getStatusCode());
		$this->assertEquals($payload['name'], $responseData['name']);
	}

	/**
	 * Test create a group category
	 */
	public function testCreateCategoryWithGroup(): void {
		$payload = [
			'name' => 'Admin\'s category test',
			'user' => sprintf('/api/v1/users/%s', self::IDS['admin_id']),
			'group' => sprintf('/api/v1/groups/%s', self::IDS['admin_group_id']),
		];

		$response = $this->makeRequest($payload);
		$responseData = $this->getResponseData($response);

		$this->assertEquals(JsonResponse::HTTP_CREATED, $response->getStatusCode());
		$this->assertEquals($payload['name'], $responseData['name']);
		$this->assertEquals($payload['group'], $responseData['group']);
	}

	/**
	 * Test forbidden access to create a category as another user
	 */
	public function testCreateCategoryForAnotherUser(): void {
		$payload = [
			'name' => 'Admin\'s category test',
			'user' => sprintf('/api/v1/users/%s', self::IDS['user_id']),
		];

		$response = $this->makeRequest($payload);
		$this->assertEquals(JsonResponse::HTTP_FORBIDDEN, $response->getStatusCode());
	}

	/**
	 * Test forbidden access to create another group category
	 */
	public function testCreateCategoryForAnotherGroup(): void {
		$payload = [
			'name' => 'Admin\'s category test2',
			'user' => sprintf('/api/v1/users/%s', self::IDS['admin_id']),
			'group' => sprintf('/api/v1/groups/%s', self::IDS['user_group_id']),
		];

		$response = $this->makeRequest($payload);

		$this->assertEquals(JsonResponse::HTTP_FORBIDDEN, $response->getStatusCode());
	}

	/**
	 * Make a request given a payload
	 *
	 * @param array $payload
	 *
	 * @return Response
	 */
	private function makeRequest(array $payload): Response {
		self::$admin->request(
			'POST',
			sprintf('%s.%s', $this->endpoint, self::FORMAT),
			[], [], [],
			json_encode($payload));
		return self::$admin->getResponse();
	}
}
