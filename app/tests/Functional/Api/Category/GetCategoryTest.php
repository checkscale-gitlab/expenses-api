<?php

namespace App\Tests\Functional\Api\Category;

use Symfony\Component\HttpFoundation\JsonResponse;

class GetCategoryTest extends CategoryTestBase {

	/**
	 * Test get all categories collection only allowed by admins
	 */
	public function testGetCategoriesWithAdmin(): void {
		self::$admin->request(
			'GET',
			sprintf('%s.%s', $this->endpoint,self::FORMAT)
		);

		$response = self::$admin->getResponse();
		$responseData = $this->getResponseData($response);

		$this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
		$this->assertCount(4, $responseData['hydra:member']);
	}

	/**
	 * Test get a user collection with admin user
	 */
	public function testGetUserCategoryWithAdmin(): void {
		self::$admin->request(
			'GET',
			sprintf(
				'%s/%s.%s',
				$this->endpoint,
				self::IDS['user_category_id'],
				self::FORMAT)
		);

		$response = self::$admin->getResponse();
		$responseData = $this->getResponseData($response);

		$this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
		$this->assertEquals($responseData['id'], self::IDS['user_category_id']);
	}

	/**
	 * Test forbidden access to get all categories with a normal user
	 */
	public function testGetCategoriesWithUser(): void {
		self::$user->request(
			'GET',
			sprintf('%s.%s', $this->endpoint, self::FORMAT)
		);

		$response = self::$user->getResponse();

		$this->assertEquals(
			JsonResponse::HTTP_FORBIDDEN, $response->getStatusCode()
		);
	}

	/**
	 * Test forbidden access to get another user category
	 */
	public function testGetAnotherUserCategory(): void {
		self::$user->request(
			'GET',
			sprintf(
				'%s/%s.%s',
				$this->endpoint,
				self::IDS['admin_category_id'],
				self::FORMAT
			)
		);

		$response = self::$user->getResponse();

		$this->assertEquals(
			JsonResponse::HTTP_FORBIDDEN,
			$response->getStatusCode()
		);
	}

}
