<?php

namespace App\Tests\Functional\Api\Expense;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class DeleteExpenseTest extends ExpenseTestBase {

	/**
	 * Test delete an owned expense
	 */
	public function testDeleteExpense(): void {
		$response = $this->makeUserRequest(self::IDS['user_category_expense_id']);

		$this->assertEquals(JsonResponse::HTTP_NO_CONTENT, $response->getStatusCode());
	}

	/**
	 * Test delete an owned group expense
	 */
	public function testDeleteGroupExpense(): void {
		$response = $this->makeUserRequest(self::IDS['user_group_category_expense_id']);

		$this->assertEquals(JsonResponse::HTTP_NO_CONTENT, $response->getStatusCode());
	}

	/**
	 * Test forbidden access to delete another user expense
	 */
	public function testDeleteAnotherUserExpense(): void {
		$response = $this->makeUserRequest(self::IDS['admin_category_expense_id']);

		$this->assertEquals(JsonResponse::HTTP_FORBIDDEN, $response->getStatusCode());
	}

	/**
	 * Test forbidden access to delete another group expense
	 */
	public function testDeleteAnotherUserGroupExpense(): void {
		$response = $this->makeUserRequest(self::IDS['admin_group_category_expense_id']);

		$this->assertEquals(JsonResponse::HTTP_FORBIDDEN, $response->getStatusCode());
	}

	/**
	 * Make a delete request given an id
	 *
	 * @param $id
	 *
	 * @return Response
	 */
	private function makeUserRequest($id): Response {
		self::$user->request(
			'DELETE',
			sprintf('%s/%s.%s', $this->endpoint, $id, self::FORMAT)
		);

		return self::$user->getResponse();
	}

}
