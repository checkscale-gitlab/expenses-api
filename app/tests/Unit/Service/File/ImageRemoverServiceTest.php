<?php

use App\Entity\User;
use App\Exception\File\FileToRemoveNotFoundException;
use App\Service\File\ImageRemoverService;
use App\Tests\Unit\TestBase;
use DG\BypassFinals;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;

class ImageRemoverServiceTest extends TestBase {

	private ImageRemoverService $imageRemover;

	private FilesystemInterface $defaultStorage;

	private string $mediaPath;

	private User $user;

	public function setUp(): void {
		parent::setUp();

		// static classes init
		BypassFinals::enable();

		$this->defaultStorage =
			$this->getMockBuilder(FilesystemInterface::class)
			->disableOriginalConstructor()
			->getMock();

		/** @var LoggerInterface|MockObject $logger */
		$logger =
			$this->getMockBuilder(LoggerInterface::class)
			     ->disableOriginalConstructor()
			     ->getMock();

		$this->mediaPath =
			__DIR__ . '/../../../../src/DataFixtures/test-files/test-image-file.png';

		$this->imageRemover =
			new ImageRemoverService(
				$this->userRepository,
				$this->defaultStorage,
				$this->mediaPath,
				$logger
			);

		$this->user = new User('test-user', 'user@mail.com');
		$this->user->setAvatar('http://test.test/avatars/test-image-file.png');
	}

	/**
	 * Test nothing to remove
	 *
	 * @throws FileNotFoundException
	 */
	public function testNoAvatarToRemove(): void {
		$this->user->setAvatar(null);

		$this->user =
			$this->imageRemover->removeAvatar($this->user);

		$this->assertNull($this->user->getAvatar());
	}

	/**
	 * Test file to remove not found
	 *
	 * @throws FileNotFoundException
	 */
	public function testFileNotFound(): void {

		$this->defaultStorage
			->expects(self::exactly(1))
			->method('delete')
			->with('test-image-file.png')
			->willReturn(false);

		$this->expectException(FileToRemoveNotFoundException::class);

		$this->imageRemover->removeAvatar($this->user);

	}

	/**
	 * Test avatar remove
	 *
	 * @throws FileNotFoundException
	 */
	public function testSuccessAvatarRemove(): void {

		$this->defaultStorage
			->expects(self::exactly(1))
			->method('delete')
			->with('test-image-file.png')
			->willReturn(true);

		$this->userRepository
			->expects(self::exactly(1))
			->method('save')
			->with($this->isType('object'));

		$this->imageRemover->removeAvatar($this->user);
		$this->assertNull($this->user->getAvatar());
	}
}
