<?php

namespace App\Tests\Unit\Api\Action\User;

use App\Api\Action\Group\AddUser;
use App\Entity\User;
use App\Service\Group\GroupService;
use App\Tests\Unit\TestBase;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AddUserTest extends TestBase {

	private AddUser $action;

	/**
	 * @var GroupService|MockObject
	 */
	private GroupService $groupService;

	/**
	 * @inheritDoc
	 */
	public function setUp(): void {

		parent::setUp();

		$this->groupService =
			$this->getMockBuilder(GroupService::class)
			->disableOriginalConstructor()
			->getMock();

		// init the register action
		$this->action =
			new AddUser(
				$this->groupService
			);
	}

	/**
	 * Test add user to group action
	 *
	 * @throws \Exception
	 */
	public function testAddUser(): void {

		$payload = [
			'group_id' => 'group_id_01',
			'user_id' => 'new_user_id_01'
		];

		$request = new Request(
			[], [], [], [], [], [],
			json_encode($payload)
		);

		$user = new User('old_user', 'old_user@mail.com');

		// no user found in the system
		$this->groupService
			->expects(self::exactly(1))
			->method('addUserToGroup')
			->with($payload['group_id'], $payload['user_id'], $user);

		$response = $this->action->__invoke($request, $user);

		$this->assertEquals(JsonResponse::HTTP_CREATED, $response->getStatusCode());
	}
}
