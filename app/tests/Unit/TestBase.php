<?php

namespace App\Tests\Unit;

use App\Repository\GroupRepository;
use App\Repository\UserRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class TestBase extends TestCase {

	/** @var UserRepository|MockObject */
	protected UserRepository $userRepository;
	/** @var GroupRepository|MockObject */
	protected GroupRepository $groupRepository;

	protected const IDS = [
		'admin_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57001',
		'user_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57002',
		'user_no_active_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57013',
		'admin_group_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57003',
		'user_group_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57004',
		'admin_category_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57005',
		'user_category_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57006',
		'admin_group_category_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57007',
		'user_group_category_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57008',
		'admin_category_expense_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57009',
		'user_category_expense_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57010',
		'admin_group_category_expense_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57011',
		'user_group_category_expense_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57010'
	];

	/**
	 * @inheritDoc
	 */
	public function setUp(): void {

		$this->userRepository =
			$this->getMockBuilder(UserRepository::class)
				->disableOriginalConstructor()->getMock();

		$this->groupRepository =
			$this->getMockBuilder(GroupRepository::class)
			     ->disableOriginalConstructor()->getMock();
	}
}
