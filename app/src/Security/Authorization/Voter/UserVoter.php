<?php

namespace App\Security\Authorization\Voter;

use App\Entity\User;
use App\Security\Role;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class UserVoter extends BaseVoter{

	public const USER_READ = 'USER_READ';
	public const USER_UPDATE = 'USER_UPDATE';
	public const USER_DELETE = 'USER_DELETE';

	/**
	 * Control security if is an action supported
	 *
	 * @param string $attribute
	 * @param mixed  $subject
	 *
	 * @return bool
	 */
	protected function supports(string $attribute, $subject) {
		return in_array($attribute, $this->getSupportedAttributes(), true);
	}

	/**
	 * Check if the current user is an admin or the user himself
	 *
	 * @param string $attribute
	 * @param User|null $subject
	 * @param TokenInterface $token
	 *
	 * @return bool
	 */
	protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool {
		/** @var User $tokenUser */
		$tokenUser = $token->getUser();

		// read all users only allowed by admins
		if (self::USER_READ === $attribute) {
			if ($subject === null){
				return $this->security->isGranted(Role::ROLE_ADMIN);
			}

			// read a user's data, allowed only for admin or by the user himself
			return $this->security->isGranted(Role::ROLE_ADMIN) ||
			       $subject->equals($tokenUser);
		}

		// update or delete a user's data,
		// allowed only for administrators or by the user himself
		if (in_array($attribute, [self::USER_UPDATE, self::USER_DELETE])) {
			return $this->security->isGranted(Role::ROLE_ADMIN) ||
			       $subject->equals($tokenUser);
		}

		// in other case return false
		return false;
	}

	private function getSupportedAttributes(): array {
		return [
			self::USER_READ,
			self::USER_UPDATE,
			self::USER_DELETE,
		];
	}
}
