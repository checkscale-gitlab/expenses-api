<?php
declare(strict_types=1);

namespace App\Security\Authorization\Voter;

use App\Entity\Category;
use App\Entity\User;
use App\Security\Role;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class CategoryVoter extends BaseVoter {

	private const CATEGORY_READ = 'CATEGORY_READ';
	private const CATEGORY_CREATE = 'CATEGORY_CREATE';
	private const CATEGORY_UPDATE = 'CATEGORY_UPDATE';
	private const CATEGORY_DELETE = 'CATEGORY_DELETE';

	/**
	 * @inheritDoc
	 */
	protected function supports(string $attribute, $subject): bool {
		return in_array($attribute, $this->getSupportedAttributes(), true);
	}

	/**
	 * Checks if the current user is an admin or the category is owned by the user,
	 * group or is member of this group
	 *
	 * @param string $attribute
	 * @param Category|null $subject
	 * @param TokenInterface $token
	 *
	 * @return bool|void
	 */
	protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool {

		/** @var User $tokenUser */
		$tokenUser = $token->getUser();

		if (self::CATEGORY_READ === $attribute) {
			if ($subject === null) {
				return $this->security->isGranted(Role::ROLE_ADMIN);
			}

			if ($subject->getGroup() !== null) {

				return $this->security->isGranted(Role::ROLE_ADMIN) ||
				       $this->groupRepository->userIsMember($subject->getGroup(), $tokenUser);
			}

			return $this->security->isGranted(Role::ROLE_ADMIN) ||
			       $subject->isOwnedBy($tokenUser);
		}

		if (self::CATEGORY_CREATE === $attribute) {
			return true;
		}

		if (self::CATEGORY_UPDATE === $attribute || self::CATEGORY_DELETE === $attribute) {
			if ($subject->getGroup() !== null) {
				return $this->security->isGranted(Role::ROLE_ADMIN) ||
				       $this->groupRepository->userIsMember($subject->getGroup(), $tokenUser);
			}

			return $this->security->isGranted(Role::ROLE_ADMIN) ||
			       $subject->isOwnedBy($tokenUser);
		}

		return false;
	}

	private function getSupportedAttributes(): array {
		return [
			self::CATEGORY_CREATE,
			self::CATEGORY_READ,
			self::CATEGORY_UPDATE,
			self::CATEGORY_DELETE
		];
	}
}
