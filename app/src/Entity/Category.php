<?php

namespace App\Entity;

use DateTime;
use Ramsey\Uuid\Uuid;

class Category {

	private string $id;
	private string $name;
	private ?User $user;
	private ?Group $group;

	private DateTime $createdAt;
	private DateTime $updatedAt;

	/**
	 * Group constructor.
	 *
	 * @param string $name
	 * @param User|null $user
	 * @param Group|null $group
	 * @param string|null $id
	 *
	 */
	public function __construct(string $name, User $user = null, Group $group = null, string $id = null) {
		$this->id = $id ?? Uuid::uuid4()->toString();

		$this->name = $name;
		$this->user = $user;
		$this->group = $group;

		$this->createdAt = new DateTime();
		$this->markAsUpdated();
	}

	/**
	 * @return string
	 */
	public function getId(): string {
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name): void {
		$this->name = $name;
	}

	/**
	 * @return User|null
	 */
	public function getUser(): ?User {
		return $this->user;
	}

	/**
	 * @return Group|null
	 */
	public function getGroup(): ?Group {
		return $this->group;
	}

	/**
	 * @return DateTime
	 */
	public function getCreatedAt(): DateTime {
		return $this->createdAt;
	}

	public function markAsUpdated(): void {
		$this->updatedAt = new DateTime();
	}

	/**
	 * @return DateTime
	 */
	public function getUpdatedAt(): DateTime {
		return $this->updatedAt;
	}

	public function isOwnedBy(User $user): bool {
		return $this->getUser() !== null &&
		       $this->getUser()->getId() === $user->getId();
	}

	public function isOwnedByGroup(Group $group): bool {
		if ($group !== null && $this->getGroup() !== null) {
			return $this->getGroup()->getId() === $group->getId();
		}

		return false;
	}
}
