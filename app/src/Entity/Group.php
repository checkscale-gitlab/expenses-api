<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Ramsey\Uuid\Uuid;

class Group {

	private ?string $id;
	private string $name;
	private User $owner;
	private DateTime $createdAt;
	private DateTime $updatedAt;

	/** @var Collection|User[]  */
	private Collection $users;

	/** @var Collection|Category[]  */
	protected ?Collection $categories = null;

	/** @var Collection|Expense[]  */
	protected ?Collection $expenses = null;

	/**
	 * Group constructor.
	 *
	 * @param string           $name
	 * @param User $owner
	 * @param string|null      $id
	 *
	 * @throws \Exception
	 */
	public function __construct(string $name, User $owner, string $id = null) {
		$this->id = $id ?? Uuid::uuid4()->toString();

		$this->name = $name;
		$this->owner = $owner;

		$this->createdAt = new DateTime();
		$this->markAsUpdated();

		$this->users = new ArrayCollection();
		$this->categories = new ArrayCollection();
		$this->expenses = new ArrayCollection();
	}

	/**
	 * @return string|null
	 */
	public function getId(): ?string {
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name): void {
		$this->name = $name;
	}

	/**
	 * @return User
	 */
	public function getOwner(): User {
		return $this->owner;
	}

	/**
	 * @return DateTime
	 */
	public function getCreatedAt(): DateTime {
		return $this->createdAt;
	}

	/**
	 * @return DateTime
	 */
	public function getUpdatedAt(): DateTime {
		return $this->updatedAt;
	}

	public function markAsUpdated(): void {
		$this->updatedAt = new DateTime();
	}

	/**
	 * @return Collection|User[]
	 */
	public function getUsers(): Collection{
		return $this->users;
	}

	public function addUser(User $user): void {
		$this->users->add($user);
		$user->addGroup($this);
	}

	public function removeUser(User $user): void {
		$this->users->removeElement($user);
		$user->removeGroup($this);
	}

	public function isOwnedBy(User $user): bool {
		return $this->getOwner()->getId() === $user->getId();
	}

	/**
	 * @return Collection|Category[]
	 */
	public function getCategories(): Collection {
		return $this->categories;
	}

	public function addCategory(Category $category): void {
		$this->categories->add($category);
	}

	public function removeCategory(Category $category) {
		$this->categories->removeElement($category);
	}

	/**
	 * @return Collection|Expense[]
	 */
	public function getExpenses(): Collection {
		return $this->expenses;
	}
}
