<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200603142909 extends AbstractMigration {

    public function getDescription(): string {
        return 'Create user_group and user_group_user tables';
    }

    public function up(Schema $schema): void {
			$this->addSql('
				create table user_group (
				    id char(36) not null primary key,
				    name varchar(100) not null,
				    owner_id char(36) not null,
				    created_at DATETIME NOT NULL,
            updated_at DATETIME NOT NULL,
            index idx_user_group_user_id (owner_id),
            constraint fk_user_group_user_id 
                foreign key (owner_id) references user (id) 
                    on update cascade on delete cascade 
				) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE = InnoDB
			');

	    $this->addSql('
	      create table user_group_user (
	          user_id char(36) not null,
	          group_id char(36) not null,
	          unique (user_id, group_id),
	          index idx_user_group_user_user_id (user_id),
	          index idx_user_group_user_group_id (group_id),
	          constraint fk_user_group_user_user_id foreign key (user_id) references user (id) on update cascade on delete cascade,
	          constraint fk_user_group_user_group_id foreign key (group_id) references user_group (id) on update cascade on delete cascade
	      ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE = InnoDB
	    ');
    }

    public function down(Schema $schema): void {
	    $this->addSql('drop table user_group_user');
	    $this->addSql('drop table user_group');
    }
}
