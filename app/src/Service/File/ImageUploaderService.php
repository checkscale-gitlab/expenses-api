<?php

namespace App\Service\File;

use App\Entity\User;
use App\Exception\File\InputFileNotFoundException;
use App\Exception\File\InvalidFileFormatException;
use App\Repository\UserRepository;
use League\Flysystem\AdapterInterface;
use League\Flysystem\FileExistsException;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;

class ImageUploaderService {

	private const INPUT_NAME = 'avatar';
	private const VALID_MIME_TYPES = ['image/png', 'image/jpeg'];

	private UserRepository $userRepository;
	private FilesystemInterface $defaultStorage;
	private LoggerInterface $logger;
	private string $mediaPath;

	public function __construct(
		UserRepository $userRepository,
		FilesystemInterface $defaultStorage,
		string $mediaPath,
		LoggerInterface $logger) {
		$this->userRepository = $userRepository;
		$this->defaultStorage = $defaultStorage;
		$this->mediaPath = $mediaPath;
		$this->logger = $logger;
	}

	/**
	 * @param Request $request
	 * @param User $user
	 *
	 * @return User
	 * @throws \Exception
	 */
	public function uploadAvatar(Request $request, User $user): User{

		/** @var File $file */
		$file = $request->files->get(self::INPUT_NAME);

		if ($file === null) {
			throw InputFileNotFoundException::fromFileName(self::INPUT_NAME);
		}

		if (!in_array($file->getMimeType(), self::VALID_MIME_TYPES)) {
			throw InvalidFileFormatException::create();
		}

		$urlExplode = explode('/', $user->getAvatar());
		$actualFileName = end($urlExplode);

		// if the avatar is null in the db dummy-avatar or empty is given
		if ($actualFileName === '' ||
		    $actualFileName === 'dummy-avatar.png') {
			return $this->createAvatar($user, $file);
		}

		$this->updateAvatar($actualFileName, $file);
		
		return $user;
	}

	/**
	 * Create and set user avatar file a given image uploaded
	 * 
	 * @param User $user
	 * @param File $file
	 *                  
	 * @return User
	 * @throws FileExistsException
	 */
	private function createAvatar(User $user, File $file): User {
		$fileName = sprintf('%s.%s', Uuid::uuid4()
		                                 ->toString(), $file->guessExtension());

		$this->defaultStorage->writeStream(
			$fileName,
			fopen($file->getPathname(), 'r'),
			['visibility' => AdapterInterface::VISIBILITY_PUBLIC]
		);

		$user->setAvatar(sprintf('%s%s', $this->mediaPath, $fileName));

		$this->userRepository->save($user);

		return $user;
	}

	/**
	 * Update an avatar file with a new one
	 *
	 * @param string $actualFileName
	 * @param File $file
	 *
	 * @throws FileNotFoundException
	 */
	private function updateAvatar(string $actualFileName, File $file): void {
		$this->defaultStorage->updateStream(
			$actualFileName,
			fopen($file->getPathname(), 'r'),
			['visibility' => AdapterInterface::VISIBILITY_PUBLIC]
		);
	}
}
