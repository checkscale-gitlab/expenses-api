<?php

namespace App\Service\Group;

use App\Entity\Group;
use App\Entity\User;
use App\Exception\Group\CannotManageGroupException;
use App\Exception\Group\IncorrectGroupException;
use App\Exception\Group\UserAlreadyInGroupException;
use App\Exception\Group\UserIsNotInGroupException;
use App\Exception\Group\UserNotFoundException;
use App\Repository\GroupRepository;
use App\Repository\UserRepository;

class GroupService {

	private UserRepository $userRepository;
	private GroupRepository $groupRepository;

	public function __construct(UserRepository $userRepository, GroupRepository $groupRepository) {
		$this->userRepository = $userRepository;
		$this->groupRepository = $groupRepository;
	}

	/**
	 * Add an user to a group if the request user is a member
	 *
	 * @param string $groupId
	 * @param string $userId
	 * @param User $user
	 *
	 * @throws \Exception
	 */
	public function addUserToGroup(string $groupId, string $userId, User $user): void {

		$group = $this->getGroupById($groupId);
		$userToAdd = $this->getUserById($userId);

		$this->checkUserMembership($group, $user);
		$this->checkUserAlreadyAdded($group, $userToAdd);

		$group->addUser($userToAdd);
		$this->groupRepository->save($group);

	}

	/**
	 * Remove an user to a group if the request user is a member
	 *
	 * @param string $groupId
	 * @param string $userId
	 * @param User $user
	 *
	 * @throws \Exception
	 */
	public function removeUserFromGroup(string $groupId, string $userId, User $user): void {

		$group = $this->getGroupById($groupId);
		$userToRemove = $this->getUserById($userId);

		$this->checkUserMembership($group, $user);

		if (!$this->groupRepository->userIsMember($group, $userToRemove)) {
			throw UserIsNotInGroupException::create();
		}

		$group->removeUser($userToRemove);
		$this->groupRepository->save($group);

	}

	/**
	 * Get a group given an id
	 *
	 * @param string $groupId
	 *
	 * @return Group
	 *
	 * @throws \Exception
	 */
	private function getGroupById(string $groupId): Group {

		$group = $this->groupRepository->findOneById($groupId);

		if ($group === null) {
			// security check
			throw IncorrectGroupException::create();
		}

		return $group;

	}

	/**
	 * Check if a user is in a group
	 *
	 * @param Group $group
	 * @param User  $user
	 *
	 * @throws \Exception
	 */
	private function checkUserMembership(Group $group, User $user): void {

		if (!$this->groupRepository->userIsMember($group, $user)) {
			throw CannotManageGroupException::create();
		}

	}

	/**
	 * Get a user given an id
	 *
	 * @param string $userId
	 *
	 * @return User
	 *
	 * @throws \Exception
	 */
	private function getUserById(string $userId): User {

		$user = $this->userRepository->findOneById($userId);

		if ($user === null) {
			throw UserNotFoundException::create();
		}

		return $user;

	}

	/**
	 * Check if an user is already in a group
	 *
	 * @param Group $group
	 * @param User  $user
	 *
	 * @throws \Exception
	 */
	private function checkUserAlreadyAdded(Group $group, User $user): void {

		if ($this->groupRepository->userIsMember($group, $user)) {
			throw UserAlreadyInGroupException::create();
		}

	}
}
