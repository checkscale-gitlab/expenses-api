<?php

namespace App\Exception\Group;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class IncorrectGroupException extends BadRequestHttpException {

	public const MESSAGE = 'The specified group doesn\'t exist or are incorrect';

	public static function create(): self {
		throw new self(self::MESSAGE);
	}
}
