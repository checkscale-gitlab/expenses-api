<?php

namespace App\Exception\User;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class CannotChangeActiveStatusException extends AccessDeniedHttpException {
	private const MESSAGE = 'Only an administrator can change the user status';

	public static function create(): self {
		throw new self(self::MESSAGE);
	}
}
