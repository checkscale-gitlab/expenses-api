<?php

namespace App\Exception\User;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class UserAlreadyExistsException extends BadRequestHttpException {
	private const MESSAGE = 'User with email %s already exist';

	public static function fromUserEmail($email): self {
		throw new self(sprintf(self::MESSAGE, $email));
	}
}
