<?php

namespace App\Api\Listener\Category;

use App\Api\Listener\PreWriteListener;
use App\Entity\Category;
use App\Entity\User;
use App\Exception\Common\CannotAddAnotherOwnerException;
use App\Exception\Group\UserIsNotInGroupException;
use App\Repository\GroupRepository;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CategoryPreWriteListener implements PreWriteListener {

	private const CATEGORY_POST = 'api_categories_post_collection';

	private TokenStorageInterface $tokenStorage;
	private GroupRepository $groupRepository;

	public function __construct(TokenStorageInterface $tokenStorage, GroupRepository $groupRepository) {
		$this->tokenStorage = $tokenStorage;
		$this->groupRepository = $groupRepository;
	}

	public function onKernelView(ViewEvent $event): void {

		$request = $event->getRequest();

		// create a category
		if ($request->get('_route') === self::CATEGORY_POST) {

			/** @var User $tokenUser */
			$tokenUser = $this->tokenStorage->getToken()->getUser();

			/** @var Category $category */
			$category = $event->getControllerResult();

			// if there is a group we have to check if the user is member
			// or the logged user is the owner
			if ($category->getGroup() !== null) {

				if (!$this->groupRepository->userIsMember($category->getGroup(), $tokenUser)) {
					throw UserIsNotInGroupException::create();
				}

				if ($category->getUser()->getId() !== $tokenUser->getId()) {
					throw CannotAddAnotherOwnerException::create();
				}

				return;
			}

			// if there is not a group then check if the logged user is the owner
			if ($category->getUser()->getId() !== $tokenUser->getId()) {
				throw CannotAddAnotherOwnerException::create();
			}
		}
	}
}
