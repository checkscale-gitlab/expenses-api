<?php

namespace App\Api\Action\User;

use App\Api\RequestTransformer;
use App\Entity\User;
use App\Exception\User\UserAlreadyExistsException;
use App\Message\Notification\Notification;
use App\Message\NotificationText;
use App\Repository\UserRepository;
use App\Security\Role;
use App\Service\Password\EncoderService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;

class Register {

	private UserRepository $userRepository;

	private MessageBusInterface $messageBus;

	private EncoderService $encoderService;

	public function __construct(
		UserRepository $userRepository,
		EncoderService $encoderService,
		MessageBusInterface $messageBus) {
		$this->userRepository = $userRepository;
		$this->messageBus = $messageBus;
		$this->encoderService = $encoderService;
	}

	/**
	 * Register a new user in the system
	 *
	 * @param Request $request
	 *
	 * @return User
	 *
	 * @throws \Exception
	 */
	public function __invoke(Request $request): User {

		// get params or throw an exception if there is any missing
		$name = RequestTransformer::getRequiredField($request, 'name');
		$email = RequestTransformer::getRequiredField($request, 'email');
		$password = RequestTransformer::getRequiredField($request, 'password');

		$existingUser = $this->userRepository->findOneByEmail($email);
		if ($existingUser !== null) {
			// if exists throw an Exception to avoid duplicates
			throw  UserAlreadyExistsException::fromUserEmail($email);
		}

		$user = new User($name, $email);

		$userCount = $this->userRepository->getUsersCount();
		if ($userCount === 0) {
			// first user is an Admin
			$user->setRoles([Role::ROLE_ADMIN]);
		}

		// encode the password
		$user->setPassword(
			$this->encoderService->generateEncodedPasswordForUser($user, $password)
		);

		$this->userRepository->save($user);

		// enqueue an email to notify the register
		$this->messageBus->dispatch(
			new Notification(
				sprintf(NotificationText::$NOTIFICATION_USER_REGISTERED, $user->getName()),
				[$user->getUsername()]
			)
		);

		return $user;
	}
}
