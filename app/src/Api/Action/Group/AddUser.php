<?php

namespace App\Api\Action\Group;

use App\Api\RequestTransformer;
use App\Entity\User;
use App\Service\Group\GroupService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AddUser {

	private GroupService $groupService;

	public function __construct(GroupService $groupService) {
		$this->groupService = $groupService;
	}

	/**
	 * Adds an user to a group
	 *
	 *
	 * @param Request $request
	 * @param User $user
	 *
	 * @return JsonResponse
	 *
	 * @throws \Exception
	 */
	public function __invoke(Request $request, User $user): JsonResponse {

		$groupId = RequestTransformer::getRequiredField($request,'group_id');
		$userId = RequestTransformer::getRequiredField($request,'user_id');

		$this->groupService->addUserToGroup($groupId, $userId, $user);

		return new JsonResponse(null, JsonResponse::HTTP_CREATED);
	}
}
